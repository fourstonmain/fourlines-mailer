<?php
	define('UPLOAD_DIR', 'project/images/');
	$img = $_POST['image-data'];
	if (strpos($img, 'data:image/png;base64,') !== false) {
    	$img = str_replace('data:image/png;base64,', '', $img);
    	$img = str_replace(' ', '+', $img);
    	$data = base64_decode($img);
    	$file = UPLOAD_DIR . uniqid() . '.png';
	}
	else {
	    $img = str_replace('data:image/jpeg;base64,', '', $img);
    	$img = str_replace(' ', '+', $img);
    	$data = base64_decode($img);
    	$file = UPLOAD_DIR . uniqid() . '.jpg';
	}
	$success = file_put_contents($file, $data);
	print $success ? $file : 'Unable to save the file.';
?>