<?php
	define('UPLOAD_DIR', 'project/');
	$code = $_POST['code'];
	$file = UPLOAD_DIR . 'index.html';
	$success = file_put_contents($file, $code);
	

    $the_folder = 'project';
    $zip_file_name = 'project.zip';
    unlink($zip_file_name);
    fopen("project.zip", "w") or die("Unable to open file!");
    class FlxZipArchive extends ZipArchive {
            /** Add a Dir with Files and Subdirs to the archive;;;;; @param string $location Real Location;;;;  @param string $name Name in Archive;;; @author Nicolas Heimann;;;; @access private  **/
        public function addDir($location, $name) {
            $this->addEmptyDir($name);
             $this->addDirDo($location, $name);
         } // EO addDir;
    
            /**  Add Files & Dirs to archive;;;; @param string $location Real Location;  @param string $name Name in Archive;;;;;; @author Nicolas Heimann * @access private   **/
        private function addDirDo($location, $name) {
            $name .= '/';         $location .= '/';
          // Read all Files in Dir
            $dir = opendir ($location);
            while ($file = readdir($dir))    {
                if ($file == '.' || $file == '..') continue;
              // Rekursiv, If dir: FlxZipArchive::addDir(), else ::File();
                $do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
                $this->$do($location . $file, $name . $file);
            }
        } 
    }

    $za = new FlxZipArchive;
    $res = $za->open($zip_file_name, ZipArchive::CREATE);
    if($res === TRUE)    {
        $za->addDir($the_folder, basename($the_folder)); $za->close();
        $filepath = "project.zip";
     if(file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename='.$filepath);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        readfile("$filepath");
     }
    }
    else  { print 'Could not create a zip archive';}
    
    
    $files = glob('project/images/*'); // get all file names
    foreach($files as $file){ // iterate files
        if(is_file($file))
        unlink($file); // delete file
    }
    
	print $success ? $file : 'Unable to save the file.';
	
	
	
?>