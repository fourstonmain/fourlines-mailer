<?php
    session_start();
    if(!isset($_SESSION['login'])) {
        header('LOCATION:login.php'); die();
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i|Roboto:300,700&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i|Roboto:300,700&amp;subset=cyrillic" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.js"></script>
    <!-- SortableJS -->
    <script src="https://unpkg.com/sortablejs@1.4.2"></script>
    <!-- VueSortable -->
    <script src="https://unpkg.com/vue-sortable@0.1.3"></script>
    <title></title>
    <style type="text/css">
      .btn-secondary.add-article-new__js, .btn-secondary.add-article-grey__js, .end-edit__js, .image-additional{
        display:none;
      }
      .btn-secondary p{
        font-size: 12px;
      }
      .btn{
          white-space: normal!important;
          font-size: 13px!important;
      }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/styles/css/style.css">
  </head>
  <body style="height:180%; overflow-x:hidden;">
    <div class="add-url">
      <h5>Добавление ссылки в абзац</h5>
      <br />
      <p style="border:1px solid cyan;" contenteditable="true" id="link">Введите ссылку</p>
      <button type="button" style="margin-bottom:20px;" id="btn" class="btn btn-info">Добавить ссылку</button>
      <hr/>
    </div>
    <div class="header">
      <div class="container">
        <div class="logo">
          <img src="assets/images/logo.png">
        </div>
        <div class="patterns">
          <img src="assets/images/patterns.png">
          Шаблоны
        </div>
        <div class="feedback">
          <img src="assets/images/feedback.png">
          Обратная связь
        </div>
        <div class="username">
          <img src="assets/images/user.png">
          4buro
        </div>
      </div>
    </div>
    <div class="patterns-preview">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <img src="assets/images/fourlines.png"/>
          </div>
          <div class="col-sm-4 rosbank">
            <img src="assets/images/resonans.png"/>
          </div>
          <div class="col-sm-4">
          </div>
        </div>
      </div>
    </div>

    <div class="container" style="width:100%;">
    <div class="row" style="height:100%">
      <div class="col-sm-4 simple_with_animation" style="margin-top:10px;">
            <button type="button"class="btn btn-primary add-article__js">Добавить новость</button>
          <button type="button" class="btn btn-primary add-adv__js">Добавить рекламный блок</button>
            <button type="button" class="btn btn-primary add-hr__js">Добавить разделительную полосу</button>
          <button type="button" class="btn btn-primary add-bottom-block__js">Добавить нижний блок</button>
        <button type="button" class="btn btn-primary add-image__js">Добавить картинку</button>
        <!--  <button type="button" class="btn btn-primary edit-frame">Добавить картинку</button> -->

        <div class="image-additional">
          <p>Загрузите картинку новости/рекламы</p>
          <form style="margin-bottom:20px;" action="actions/upload.php" method="POST" target="postiframe" id="image-form">
          <input style="margin-bottom:20px;" type='file' onchange="readURL(this);" />
          <textarea style="display:none;" name="image-data" id="image-data"></textarea>
          <input type='submit' value='Загрузить картинку'>
          </form>
          <iframe style="display:none;" id="postiframe" name="postiframe"></iframe>
          <div class="row">
              <button type="button" style="margin-bottom:10px; width: 45%; margin-right: 20px" class="btn btn-secondary add-parag__js">Добавить подпись к картинке</button>
              <button type="button" style="margin-bottom:10px; width: 45%;" class="btn btn-secondary add-parag-more__js">Добавить еще строку</button>
          </div>
        </div>

        <br />
        <hr/>
        <h5>Добавление ссылки на картинку</h5>
        <br />
        <p contenteditable="true" id="link-img">Введите ссылку</p>
        <button type="button" style="margin-bottom:20px;" id="btn-img" class="btn btn-info">Добавить ссылку</button>
        <hr/>
        <div class="row">
            <button type="button" style="margin-bottom:10px; width: 45%; margin-right: 20px" class="btn btn-secondary add-article-new__js">Добавить абзац к добавленной новости</button>
            <button type="button" style="margin-bottom:10px; width: 45%;" class="btn btn-secondary add-article-grey__js">Добавить серый абзац к добавленной новости</button>
        </div>

        <!--<button style="margin-bottom:20px;" onclick="myFunction()">Скопировать в буфер и вывести код</button>-->
        <form style="text-align:center; margin:auto; margin-bottom:20px; padding-top:20px;" id="save_archive" method="POST" action="actions/compress_and_save.php" target="postiframe">
          <textarea style="display: none; margin-top:20px; width:100%; height: 300px;" id="code" name="code"></textarea>
          <input class="btn btn-dark" onclick="myFunction()" type='submit' value='Сохранить архив'>
        </form>

        <!--<button type="button" style="margin-bottom:20px; width: 100%;" class="btn btn-success end-edit__js"><p>Закончить редактирование новости</p></button>-->
      </div>
      <div class="col-sm-8" style="height:100%; background: #e9e9e9;">
        <div class="frame-options-row">
          <div class="adapt desktop-display active">
            Desktop
          </div>
          <div class="adapt mobile-display">
            Mobile
          </div>
        </div>
      <iframe id="mail-body" src="mail.html" style="width:100%;height:1200px;">
        Ваш браузер не поддерживает фреймы
      </iframe>
  </div>
  <!--
  <div style="margin-top:10px" class="col-sm-12">
    <h3>Код письма</h3><hr/>
    <form id="save_archive" method="POST" action="actions/compress_and_save.php" target="postiframe">
      <textarea style="margin-top:10px; width:100%; height: 300px;" id="code" name="code"></textarea>
      <input type='submit' value='Сохранить архив'>
    </form>
  </div>
  -->
  </div>
</div>
<div class="about">
  <a href="about.html" class="badge badge-info">
    <p>Версия 0.0.3</p>
    <p>Flexible Bear</p>
    <p>Нажми, чтобы почитать</p>
    </a>
</div>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<!--  <script src="assets/js/libs/vue.js"></script> -->
  <script src="https://johnny.github.io/jquery-sortable/js/jquery-sortable.js"></script>
  <script src="assets/js/app.js"></script>
  <script>
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#mail-body').contents().find('body').find('#news-image').attr('src', e.target.result);
        $('#image-data').val(e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
    }
    //$("#image-form").submit();
  }
  </script>

  <script>
  function myFunction() {
    $('#mail-body').contents().find('html p').attr("contenteditable","false");
    $('#mail-body').contents().find('body').find('.close').remove();
    $('#mail-body').contents().find('body').find('td').removeClass('active');
    $('#mail-body').contents().find('body').find('td').removeClass('non-active');
    var text = $('#mail-body').contents().find('html').html();
    text = text.replace(new RegExp("&amp;",'g'),"&")
    text = text.replace(new RegExp("actions/project/",'g'),"")
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    $('#code').val(text);
    document.execCommand("copy");
    $temp.remove();
  }
  </script>


  </body>

</html>
